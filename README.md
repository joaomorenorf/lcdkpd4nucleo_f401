# LCDKPD4NUCLEO_F401
This is an API for the [LCD Keypad Shield](https://wiki.dfrobot.com/Arduino_LCD_KeyPad_Shield__SKU__DFR0009_) and [NUCLEO-F401RE board](https://www.st.com/en/evaluation-tools/nucleo-f401re.html). Its main purpose is to provide an easy to use interface for [another LCD library](https://github.com/4ilo/HD44780-Stm32HAL), adding some shield-specific functionalities (e.g. support for the integrated pushbuttons).

This API is being developed as an assignement for Embedded Systems Programming course - Prof. Ricardo de Oliveira Duarte @ DELT, Universidade Federal de Minas Gerais.

## More information
For more information, please check LCDKPD4NUCLEO_F401's [wiki](https://gitlab.com/joaomorenorf/lcdkpd4nucleo_f401/-/wikis/home). You will find info about:
* [Getting started](https://gitlab.com/joaomorenorf/lcdkpd4nucleo_f401/-/wikis/Getting-started)
* [Library documentation](https://gitlab.com/joaomorenorf/lcdkpd4nucleo_f401/-/wikis/Library-documentation)
* [Useful documents](https://gitlab.com/joaomorenorf/lcdkpd4nucleo_f401/-/wikis/Useful-Documents)
* [Troubleshooting](https://gitlab.com/joaomorenorf/lcdkpd4nucleo_f401/-/wikis/Troubleshooting)